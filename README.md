# Desafio Avanti Devops

#Arquivos de configuração do terraform estão dentro da pasta terraform_files

### Comando Terraform Plan
<img src="prints/terraform_plan.png">

### Comando Terraform apply
<img src="prints/terraform_apply.png">

### Máquina na AWS
<img src="prints/maquina_aws.png">


### dada a permissão pro gitlab-runner
<img src="prints/runner.png">

### register do runner
<img src="prints/runner_register.png">


### pipelines
<img src="prints/pipelines.png">

### print da aplicação complexa
<img src="prints/aplicacao.png" >
